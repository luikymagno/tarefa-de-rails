class Reservation < ApplicationRecord
    has_one :librarian
    has_one :client
    has_one :book
end
