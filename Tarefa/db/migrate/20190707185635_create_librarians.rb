class CreateLibrarians < ActiveRecord::Migration[5.0]
  def change
    create_table :librarians do |t|
      t.string :name
      t.references :reservation, foreign_key: true

      t.timestamps
    end
  end
end
